﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LKEngine
{
    public class Camera
    {
        public int width;
        public int height;
        public int x;
        public int y;

        public float zoom;


        public Camera(int width, int height, int x, int y)
        {
            this.width = width;
            this.height = height;
            this.x = x;
            this.y = y;
            zoom = .2f;
        }


        public Matrix matrix => Matrix.CreateTranslation(-x, -y, 0) * Matrix.CreateScale(zoom) * Matrix.CreateTranslation(Engine.viewWidth/2f, Engine.viewHeight/2f, 0);
    }
}
