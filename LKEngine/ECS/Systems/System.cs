﻿using System.Collections.Generic;
using LKEngine.Utils;

namespace LKEngine.ECS
{
    public abstract class System
    {
        private readonly BitSet componentBitSet;
        public HashSet<int> interestedEntities = new HashSet<int>();

        public Scene scene;

        /// <param name="componentBitSet">BitSet containing values corresponding to the component types according to EntityComponentManager.componentTypes</param>
        protected System(BitSet componentBitSet = null)
        {
            this.componentBitSet = componentBitSet;
        }

        /// <summary>
        /// Update is called for every System in the order defined in the SystemManager logicSystems and drawSystems arrays' initialization.
        /// interestedEntities contents may be used to update entities the system is interested in.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Registers entities to be updated by the system if their components BitSets match
        /// </summary>
        /// <param name="entity"></param>
        public void CheckInterest(int entity)
        {
            if(componentBitSet == null)
                return;
            if (componentBitSet.IsContainedIn(scene.entityComponentManager.entitySets[entity]))
            {
                interestedEntities.Add(entity);
            }
            else
            {
                interestedEntities.Remove(entity);
            }
        }
    }
}
