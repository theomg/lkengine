﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace LKEngine.ECS
{
    class ClearSystem : System
    {
        public ClearSystem() : base(EntityComponentManager.MakeComponentBitset(
            typeof(PositionComponent))){}

        private PositionComponent pos;
        private Rectangle bounds = new Rectangle(0, 0, 1280, 720);

        private List<int> markedEntities = new List<int>();

        public override void Update()
        {
            foreach (var entity in interestedEntities)
            {
                var pos = scene.entityComponentManager.components.positionComponents[entity];
                if (pos.x < 0 || pos.x > 1280 || pos.y < 0 || pos.y > 720)
                {
                    markedEntities.Add(entity);
                }
            }
            for (var i = markedEntities.Count - 1; i >= 0; i--)
            {
                scene.entityComponentManager.RemoveEntity(markedEntities[i]);
            }
            markedEntities.Clear();
        }
    }
}
