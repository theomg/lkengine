﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LKEngine.ECS
{
    class RenderSystem : System
    {
        public RenderSystem() : base(EntityComponentManager.MakeComponentBitset(
            typeof(PositionComponent),
            typeof(SpriteComponent))){}

        private readonly SpriteBatch batch = new SpriteBatch(Engine.graphicsDevice);

        public override void Update()
        {
            batch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, scene.camera.matrix);
            foreach (var entity in interestedEntities)
            {
                var pos = scene.entityComponentManager.components.positionComponents[entity];
                var sprite = scene.entityComponentManager.components.spriteComponents[entity];
                batch.Draw(sprite.texture, new Vector2(pos.x,pos.y), Color.White);
            }
            batch.End();
        }
    }
}
