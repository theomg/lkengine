﻿using System;

namespace LKEngine.ECS
{
    class MovementSystem : System
    {
        public MovementSystem() : base(EntityComponentManager.MakeComponentBitset(
            typeof(PositionComponent),
            typeof(VelocityComponent))){}

        private PositionComponent pos;
        private VelocityComponent vel;

        private int roundedX;
        private int roundedY;

        public override void Update()
        {
            foreach (var entity in interestedEntities)
            {
                pos = scene.entityComponentManager.components.positionComponents[entity];
                vel = scene.entityComponentManager.components.velocityComponents[entity];

                pos.remainderX += vel.x * Engine.deltaTime;
                pos.remainderY += vel.y * Engine.deltaTime;

                roundedX = (int)Math.Round(pos.remainderX);
                roundedY = (int)Math.Round(pos.remainderY);

                pos.x += roundedX;
                pos.remainderX -= roundedX;

                pos.y += roundedY;
                pos.remainderY -= roundedY;


            }
        }
    }
}
