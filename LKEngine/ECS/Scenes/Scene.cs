﻿using System;
using System.Collections;
using LKEngine.ECS;
using Microsoft.Xna.Framework.Content;

namespace LKEngine
{
    public class Scene
    {
        public EntityComponentManager entityComponentManager;
        public SystemManager systemManager;
        public ContentManager content;
        public Camera camera;


        public Scene()
        {
            Initialize();
        }

        private void Initialize()
        {
            entityComponentManager = new EntityComponentManager(this);
            systemManager = new SystemManager(this);
            content = new ContentManager(Engine.services, Engine.content.RootDirectory);
            camera = new Camera(Engine.viewWidth, Engine.viewHeight, Engine.viewWidth/2, Engine.viewHeight/2);
        }

        public virtual void Update()
        {
            systemManager.UpdateLogicSystems();
        }

        public virtual void Draw()
        {
            systemManager.UpdateDrawSystems();
        }

        public virtual void OnBegin(){}

        public virtual void OnEnd(){}
    }
}