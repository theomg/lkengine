﻿using System;
using System.Collections;
using LKEngine.ECS;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LKEngine
{
    public sealed class TestScene : Scene
    {
        public TestScene() : base()
        {
            Initialize();
        }

        private Texture2D spr;
        private Random rand;

        private void Initialize()
        {
            spr = content.Load<Texture2D>(Contents.Sprites.A);
            rand = new Random();
            for (float i = 0; i < Engine.graphics.PreferredBackBufferHeight; i += 4f)
            {
                for (float j = 0; j < Engine.graphics.PreferredBackBufferWidth; j += 4f)
                {
                    var ent = entityComponentManager.AddEntity();
                    entityComponentManager.AddComponent<SpriteComponent>(ent).SetSprite(spr);
                    entityComponentManager.AddComponent<PositionComponent>(ent).SetPosition((int)j, (int)i);
                    entityComponentManager.AddComponent<VelocityComponent>(ent).SetVelocity(rand.Next(-50, 50), rand.Next(-50, 50));
                }
            }
        }

        public override void Update()
        {
            base.Update();
            for (var i = 0; i < 34; i++)
            {
                var ent = entityComponentManager.AddEntity();
                entityComponentManager.AddComponent<SpriteComponent>(ent).SetSprite(spr);
                entityComponentManager.AddComponent<PositionComponent>(ent).SetPosition(640, 360);
                entityComponentManager.AddComponent<VelocityComponent>(ent).SetVelocity(rand.Next(-50, 50), rand.Next(-50, 50));
            }
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override void OnBegin()
        {
            var cor = Engine.StartCoroutine(Printer());
            Engine.StopCoroutine(cor);
            Engine.StartCoroutine(Printer());
        }

        IEnumerator Printer()
        {
            var lastTime = 0f;
            while (true)
            {
                lastTime += Engine.realDeltaTime;
                if (lastTime > 1f)
                {
                    lastTime = 0;
                    Console.WriteLine("la1");
                    yield return CoroutineManager.WaitForSeconds(2f);
                    Console.WriteLine("la2");
                }
                yield return null;
            }
        }

        public override void OnEnd(){}
    }
}
