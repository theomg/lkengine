﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using LKEngine.Utils;

namespace LKEngine.ECS
{
    public class EntityComponentManager
    {
        public static List<Type> componentTypes = new List<Type>();

        public List<BitSet> entitySets = new List<BitSet>();
        private Queue<int> availableEntities = new Queue<int>();
        
        public Components components = new Components();

        private readonly Scene scene;

        public EntityComponentManager(Scene scene)
        {
            Initialize();
            this.scene = scene;
        }

        private void Initialize()
        {
            foreach (var type in Assembly.GetEntryAssembly().GetTypes())
            {
                if (!typeof(IComponent).IsAssignableFrom(type) || !type.IsClass) continue;
                componentTypes.Add(type);
            }
        }

        #region Entity Methods
        public int AddEntity()
        {
            if (availableEntities.Count > 0)
            {
                var entity = availableEntities.Dequeue();
                entitySets[entity] = new BitSet();
                return entity;
            }
            entitySets.Add(new BitSet());
            return entitySets.Count - 1;
        }

        public void RemoveEntity(int entity)
        {
            RemoveAllComponents(entity);

            foreach (var logicSystem in scene.systemManager.logicSystems)
            {
                logicSystem.interestedEntities.Remove(entity);
            }

            foreach (var drawSystem in scene.systemManager.drawSystems)
            {
                drawSystem.interestedEntities.Remove(entity);
            }
            entitySets[entity] = null;
            availableEntities.Enqueue(entity);
        }
#endregion

#region Component Methods

        public T AddComponent<T>(int entity) where T : IComponent, new()
        {
            var component = new T();
            var type = typeof(T);

            entitySets[entity].SetBit(componentTypes.IndexOf(type));

            foreach (var logicSystem in scene.systemManager.logicSystems)
            {
                if (!logicSystem.interestedEntities.Contains(entity))
                {
                    logicSystem.CheckInterest(entity);
                }
            }

            foreach (var drawSystem in scene.systemManager.drawSystems)
            {
                if (!drawSystem.interestedEntities.Contains(entity))
                {
                    drawSystem.CheckInterest(entity);
                }
            }

            //If the component list is large enough you can simply assign the value, else you insert to resize the list.
            if (entity < components.componentLists[type].Count)
            {
                components.componentLists[type][entity] = component;
            }
            else
            {
                components.componentLists[type].Insert(entity, component);
            }
            return component;
        }

        public void RemoveComponent<T>(int entity) where T : IComponent
        {
            var type = typeof(T);
            components.componentLists[type][entity] = null;
            entitySets[entity].ClearBit(componentTypes.IndexOf(type));
    
            foreach (var logicSystem in scene.systemManager.logicSystems)
            {
                logicSystem.CheckInterest(entity);
            }

            foreach (var drawSystem in scene.systemManager.drawSystems)
            {
                drawSystem.CheckInterest(entity);
            }
        }

        public void RemoveAllComponents(int entity)
        {
            foreach (var list in components.componentLists.Values)
            {
                list[entity] = null;
            }
        }

        public T GetComponent<T>(int entity) where T : IComponent
        {
            return (T)components.componentLists[typeof(T)][entity];
        }

        /// <summary>
        /// Helper to make a BitSet from component Types
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public static BitSet MakeComponentBitset(params Type[] types) 
        {
            var componentIndexes = new int[types.Length];
            for (var i = 0; i < types.Length; i++)
            {
                componentIndexes[i] = componentTypes.IndexOf(types[i]);
#if DEBUG
                if(componentIndexes[i] == -1)
                    Debug.Fail($"The type {types[i].Name} is not a valid component type.");
#endif
            }
            return new BitSet(componentIndexes);
        }
#endregion
    }
}
