﻿using System;
using System.Reflection;

namespace LKEngine.ECS
{
    public class SystemManager
    {
        //The order of the systems initialized in these arrays is the order they'll be updated in
        public System[] logicSystems =
        {
            new MovementSystem(),
            new ClearSystem()
        };
        public System[] drawSystems =
        {
            new RenderSystem()
        };

        private readonly Scene scene;

        public SystemManager(Scene scene)
        {
            this.scene = scene;
            Initialize();
        }

        public void UpdateLogicSystems()
        {
            foreach (var logicSystem in logicSystems)
            {
                logicSystem.Update();
            }
        }

        public void UpdateDrawSystems()
        {
            foreach (var drawSystem in drawSystems)
            {
                drawSystem.Update();
            }
        }

        private void Initialize()
        {
            foreach (var logicSystem in logicSystems)
            {
                logicSystem.scene = scene;
            }
            foreach (var drawSystem in drawSystems)
            {
                drawSystem.scene = scene;
            }
#if DEBUG
            foreach (var type in Assembly.GetEntryAssembly().GetTypes())
            {
                if (!type.IsSubclassOf(typeof(System))) continue;
                
                if(!Array.Exists(logicSystems, system => system.GetType() == type) && !Array.Exists(drawSystems, system => system.GetType() == type))
                    DebugUtils.Log($"The system {type.Name} was not defined in either array.");
            }
#endif
        }
    }
}
