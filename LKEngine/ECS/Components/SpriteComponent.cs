﻿using Microsoft.Xna.Framework.Graphics;

namespace LKEngine.ECS
{
    public class SpriteComponent : IComponent
    {
        public Texture2D texture;

        public SpriteComponent SetSprite(Texture2D texture)
        {
            this.texture = texture;
            return this;
        }
    }
}
