﻿namespace LKEngine.ECS
{
    public class VelocityComponent : IComponent
    {
        public float x;
        public float y;

        public VelocityComponent SetVelocity(float x, float y)
        {
            this.x = x;
            this.y = y;
            return this;
        }
    }
}