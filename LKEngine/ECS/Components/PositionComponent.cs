﻿namespace LKEngine.ECS
{
    public class PositionComponent : IComponent
    {
        public int x;
        public int y;

        public float remainderX;
        public float remainderY;

        public PositionComponent SetPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
            return this;
        }
    }
}
