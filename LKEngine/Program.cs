﻿using System;

namespace LKEngine
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using (var game = new Engine(1280, 720, 1, false, false, "LKEngine", false))
                game.Run();
        }
    }
#endif
}
