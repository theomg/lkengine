﻿using System;

namespace LKEngine.Utils
{
    public class BitSet : IEquatable<BitSet>
    {
        private static readonly int bitMaskLimit = 5;
        private static readonly int bitMask = (1 << bitMaskLimit) - 1;

        private int[] bits;

        public BitSet() : this(setBits: null){}

        /// <param name="setBits">Array of bits that should be set.</param>
        public BitSet(int[] setBits)
        {
            bits = new int[1];
            if (setBits == null) return;
            foreach (var bit in setBits)
            {
                SetBit(bit);
            }
        }

        public bool IsSet(int id)
        {
            int bit = id >> bitMaskLimit;
            if (bit >= bits.Length)
                return false;

            return (bits[bit] & (1 << (id & bitMask))) != 0;
        }

        public void SetBit(int id)
        {
            int bit = id >> bitMaskLimit;
            if (bit >= bits.Length)
            {
                Array.Resize(ref bits, bit+1);
            }

            bits[bit] |= 1 << (id & bitMask);
        }

        public void ClearBit(int id)
        {
            int bit = id >> bitMaskLimit;
            if (bit >= bits.Length)
                return;
            
            bits[bit] &= ~(1 << (id & bitMask));
        }

        public void ClearAllBits()
        {
            for (int i = 0; i < bits.Length; i++)
            {
                bits[i] = 0;
            }
        }

        public bool IsContainedIn(BitSet bitSet)
        {
            if (bits.Length > bitSet.bits.Length)
                return false;

            for (int i = 0; i < bits.Length; i++)
            {
                if ((bits[i] & bitSet.bits[i]) != bits[i])
                    return false;
            }
            return true;
        }

        public bool Equals(BitSet bitSet)
        {
            if (bitSet?.bits == null || bits == null)
            {
                return false;
            }

            if (bitSet.bits.Length != bits.Length)
            {
                return false;
            }

            for (var i = 0; i < bits.Length; i++)
            {
                if (bits[i] != bitSet.bits[i])
                    return false;
            }
            return true;
        }
    }
}
