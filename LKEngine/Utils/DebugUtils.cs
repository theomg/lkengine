﻿#if DEBUG
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace LKEngine
{
    public static class DebugUtils
    {
        public static void Log(string message,
            [CallerFilePath] string file = "",
            [CallerLineNumber] int line = 0,
            [CallerMemberName] string member = "")
        {
            Debug.WriteLine($"\n\t{file}({line}): {member}: {message}", "\nLKEngine");
        }
    }
}
#endif