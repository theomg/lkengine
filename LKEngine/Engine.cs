﻿using System;
using System.Collections;
using LKEngine.ECS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LKEngine
{
    public class Engine : Game
    {
#if DEBUG
        private int frameCounter;
        private TimeSpan frameTime;
#endif

        public static GraphicsDeviceManager graphics;
        public static int viewWidth;
        public static int viewHeight;
        public static float deltaTime;
        public static float realDeltaTime;
        public static float elapsedTime;
        public static float realElapsedTime;
        public static float timeScale = 1f;
        public static bool fullscreen;

        private static Color clearColor = Color.Black;
        public static string windowTitle;

        public static ContentManager content;
        public static GameServiceContainer services;
        public static GraphicsDevice graphicsDevice;

        public static CoroutineManager coroutineManager;

        private static  Scene scene;
        private static Scene nextScene;
        public static Scene Scene
        {
            get { return scene; }
            set { nextScene = value; }
        }
        
        public Engine(int width, int height, int windowScale, bool isFullscreen, bool isBorderless, string title, bool useVsync)
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.SynchronizeWithVerticalRetrace = useVsync;
            if (isFullscreen)
            {
                SetFullscreen(isBorderless);
            }
            else
            {
                SetWindowed(isBorderless, width, height, windowScale);
            }

            viewWidth = width;
            viewHeight = height;

            Content.RootDirectory = @"Content";
            Window.Title = title;
            windowTitle = title;

            content = Content;
            services = Services;

            coroutineManager = new CoroutineManager();

            IsMouseVisible = false;
            IsFixedTimeStep = false;
        }

        public void SetFullscreen(bool isBorderless)
        {
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Window.Position = new Point(0, 0);
            Window.IsBorderless = isBorderless;
            graphics.IsFullScreen = !isBorderless;
        }

        public void SetWindowed(bool isBorderless, int width, int height, int windowScale)
        {
            viewWidth = width;
            viewHeight = height;
            graphics.PreferredBackBufferWidth = width * windowScale;
            graphics.PreferredBackBufferHeight = height * windowScale;
            graphics.IsFullScreen = false;
            Window.IsBorderless = isBorderless;
        }

        protected override void Initialize()
        {
            graphicsDevice = GraphicsDevice;
            base.Initialize();

            Scene = new TestScene();
        }
        
        protected override void Update(GameTime gameTime)
        {
            realDeltaTime = (float) gameTime.ElapsedGameTime.TotalSeconds;
            deltaTime = realDeltaTime*timeScale;
            realElapsedTime += realDeltaTime;
            elapsedTime += deltaTime;

            coroutineManager.Update();
            scene?.Update();
            if (nextScene != null)
            {
                scene?.OnEnd();
                scene?.content.Unload();
                scene = nextScene;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                nextScene = null;
                scene.OnBegin();
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(clearColor);
            scene?.Draw();
            base.Draw(gameTime);
#if DEBUG
            frameCounter++;
            frameTime += gameTime.ElapsedGameTime;
            if (frameTime.TotalSeconds > 1)
            {
                frameTime -= TimeSpan.FromSeconds(1);
                Window.Title = $"{windowTitle} - {frameCounter}fps/{(1000f/frameCounter)}ms - {(GC.GetTotalMemory(false)/(1024f*1024f)):0.##}MB";
                frameCounter = 0;
            }
#endif
        }

#region Helper

        public static int StartCoroutine(IEnumerator coroutine)
        {
            return coroutineManager.Start(coroutine);
        }

        public static void StopCoroutine(int coroutineID)
        {
            coroutineManager.Stop(coroutineID);
        }

#endregion
    }
}
