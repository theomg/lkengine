﻿using System;
using System.Collections;
using System.Collections.Generic;
using LKEngine.ECS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace LKEngine
{
    public class CoroutineManager
    {
        private readonly List<IEnumerator> coroutines = new List<IEnumerator>();

        private Queue<int> unusedIDs = new Queue<int>();

        /// <summary>
        /// Adds coroutine to be updated. 
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns>Returns a unique coroutine ID.</returns>
        public int Start(IEnumerator coroutine)
        {
            if (unusedIDs.Count > 0)
            {
                coroutines[unusedIDs.Peek()] = coroutine;
                return unusedIDs.Dequeue();
            }
            else
            {
                coroutines.Add(coroutine);
                return coroutines.Count - 1;
            }
        }

        public void StopAll()
        {
            coroutines.Clear();
            unusedIDs.Clear();
        }

        public void Stop(int coroutineID)
        {
            coroutines[coroutineID] = null;
            unusedIDs.Enqueue(coroutineID);
        }

        public void Update()
        {
            for (var i = 0; i < coroutines.Count; i++)
            {
                if(coroutines[i] == null)
                    continue;
                if (coroutines[i].Current is IEnumerator && MoveNext((IEnumerator)coroutines[i].Current))
                    continue;
                if (!coroutines[i].MoveNext())
                    coroutines.RemoveAt(i--);
            }
        }

        private bool MoveNext(IEnumerator coroutine)
        {
            if (coroutine.Current is IEnumerator && MoveNext((IEnumerator)coroutine.Current))
            {
                return true;
            }
            return coroutine.MoveNext();
        }

        /// <summary>
        /// Waits for an amount of time using elapsedTime.
        /// </summary>
        /// <param name="time">How many seconds to wait</param>
        public static IEnumerator WaitForSeconds(float time)
        {
            var elapsed = Engine.elapsedTime;
            while (Engine.elapsedTime - elapsed < time)
            {
                yield return null;
            }
        }

        /// <summary>
        /// Waits for an amount of time using realElapsedTime.
        /// </summary>
        /// <param name="time">How many seconds to wait</param>
        public static IEnumerator WaitForRealSeconds(float time)
        {
            var elapsed = Engine.realElapsedTime;
            while (Engine.realElapsedTime - elapsed < time)
            {
                yield return null;
            }
        }
    }
}
